
## 0.3.0 [03-21-2023]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/security/adapter-owasp_zap!5

---