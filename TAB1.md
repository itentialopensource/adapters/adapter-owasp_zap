# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Owasp_zap System. The API that was used to build the adapter for Owasp_zap is usually available in the report directory of this adapter. The adapter utilizes the Owasp_zap API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Owasp Zap adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Owasp Zap to offer security and trust information. 

With this adapter you have the ability to perform operations with Owasp Zap such as:

- Scanners
- Policies
- Alerts

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
